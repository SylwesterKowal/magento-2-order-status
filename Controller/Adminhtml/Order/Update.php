<?php
declare(strict_types=1);

namespace Kowal\OrderStatus\Controller\Adminhtml\Order;

use Kowal\OrderStatus\Helper\AdditionalOrderStatus;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NotFoundException;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Api\OrderItemRepositoryInterface;
use Magento\Sales\Model\Order;

class Update implements HttpGetActionInterface
{
    /**
     * @var RequestInterface
     */
    private $request;
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;
    /**
     * @var ResultFactory
     */
    private $resultFactory;

    /**
     * @var OrderItemRepositoryInterface
     */
    private $orderItemRepository;

    /**
     * @param RequestInterface $request
     * @param OrderRepositoryInterface $orderRepository
     * @param OrderItemRepositoryInterface $orderItemRepository
     * @param ResultFactory $resultFactory
     */
    public function __construct(
        RequestInterface $request,
        OrderRepositoryInterface $orderRepository,
        OrderItemRepositoryInterface $orderItemRepository,
        ResultFactory $resultFactory
    ) {
        $this->request = $request;
        $this->orderRepository = $orderRepository;
        $this->orderItemRepository = $orderItemRepository;
        $this->resultFactory = $resultFactory;
    }

    /**
     * Execute action based on request and return result
     *
     * @return ResultInterface|ResponseInterface
     * @throws NotFoundException
     */
    public function execute()
    {
        $orderId = $this->request->getParam('order_id');
        $status = $this->request->getParam('status_code');
        $order = $this->orderRepository->get($orderId);
        $result = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $prevStatus = $order->getStatus();

        $result->setPath('sales/order/view/order_id/' . $orderId);

        switch ($status) {
            case AdditionalOrderStatus::STATUS_PAID:
                $order->setState(Order::STATE_PROCESSING);
                break;
            case AdditionalOrderStatus::STATUS_SENT:
                $order->setState(Order::STATE_PROCESSING);
                break;
            case AdditionalOrderStatus::STATUS_PENDING:
                $order->setState(Order::STATE_PROCESSING);
                break;
            case AdditionalOrderStatus::STATUS_PRODUKCJA:
                $order->setState(Order::STATE_PROCESSING);
                break;
            case AdditionalOrderStatus::STATUS_COMPLETE:
                $order->setState(Order::STATE_COMPLETE);
                break;
            default:
                return $result;
        }

        if ($prevStatus == Order::STATE_CANCELED || Order::STATE_CLOSED) {

            $items = $order->getItems();
            foreach ($items as $item) {
                $item->setQtyCanceled(0);
                $this->orderItemRepository->save($item);
            }
        }
        $order->setStatus($status);
        $this->orderRepository->save($order);

        return $result;
    }
}
