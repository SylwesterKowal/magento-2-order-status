<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\OrderStatus\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class AdditionalOrderStatus extends AbstractHelper
{
    public const STATUS_PAYMENT_NOT_APPROVED = 'pending';
    public const STATUS_PAID = 'processing';
    public const STATUS_SENT = 'shipped';
    public const STATUS_PENDING = 'weryfikacja';
    public const STATUS_PRODUKCJA = 'w_produkcji';
    public const STATUS_COMPLETE = 'complete';

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    ) {
        parent::__construct($context);
    }
}
